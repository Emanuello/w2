<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon"sizes="32*32" href="https://gitlab.com/dan-it/groups/pe-27/-/raw/main/markup/homework/homework2-
    Simple-Html-Page/favicon.png?ref_type=heads"/>
    <title>  What is HTML</title>
</head>

<body>
<header>
    <h1>What Is HTML?</h1>
    <img src="https://gitlab.com/dan-it/groups/pe-27/-/raw/main/markup/homework/homework2-Simple-Html-Page/html-logo.png"
         width="300" height="300"
    alt=" Html5 logotype" title="Html5 logotype"
    <main>
        <article>
<ul>
    <li>HTML stands for Hyper Text Markup Language</li>
    <li>HTML is the standard markup language for creating Web pages</li>
    <li>HTML describes the structure of a Web page</li>
    <li>HTML consists of a series of elements</li>
    <li>HTML elements tell the browser how to display the content </li>
    <li>HTML elements label pieces of content such as "this is heading", "this is a paragraph", "this is a link", etc. </li>
</ul>
        <section>
            <h2>Basics</h2>
            <p>An HTML element is defined by a <u>start</u> tag,some content,and
                an a tag.<b>The HTML element</b> is everything from the start tag to the end tag.</p>

               <p> Web browsers receive HTML documents from a web server or from local
                   storage and render the documents into multimedia web pages.
                   HTML describes the structure of a web page semantically and originally
                   included cues for the appearance of the document</p>
            <h2>History</h2>

        </section>
        <section>
            <h3>Tags</h3>
            <p> HTML headings are defined with the
            <code> &lth1> </code> to <code> &lth6> </code>  tags with H1 being the highest
                (or most important) level and H6 the least:
            </p>
        </section>
            <h1>Heading Level 1</h1>
            <h2>Heading Level 2</h2>
            <h3>Heading Level 3</h3>
            <h4>Heading Level 4</h4>
            <h5>Heading Level 5</h5>
            <h6>Heading Level 6</h6>
        </article>
    </main>
</header>
<footer>
    <p>Read more <a href="https://dan-it.gitlab.io/fe-book-ua/programming_essentials/html_css/ext_html_
    basic/ext_html_basic.html"
                    target="_blank"> Here</a>
    </p>
    <h5>Have a questions? Write here</h5>
    <input placeholder="Your email" > <button> Send </button>
</footer>
</body>
</html>